import SwiftUI

let accentColorOptions = [
    ("Red", Color.red),
    ("Blue", Color.blue),
    ("Yellow", Color.yellow),
    ("Orange", Color.orange),
    ("Purple", Color.purple),
    ("Green", Color.green)
]

class AccentColorSelection: ObservableObject {
    @Published var selectedColor: Color
    
    init(selectedColor: Color) {
        self.selectedColor = selectedColor
    }
}

struct AccentColourView: View {
    @EnvironmentObject var accentColorSelection: AccentColorSelection
    let accentColorOptions: [(String, Color)]
    weak var delegate: AccentColorDelegate?

    var body: some View {
        NavigationView {
            List {
                ForEach(accentColorOptions, id: \.0) { option in
                    AccentColorItem(name: option.0, color: option.1)
                        .onTapGesture {
                            delegate?.didSelectAccentColor(option.1)
                            accentColorSelection.selectedColor = option.1
                            UIApplication.shared.windows.first?.rootViewController?.view.tintColor = UIColor(option.1)
                        }
                        .foregroundColor(accentColorSelection.selectedColor == option.1 ? .blue : .primary)
                }
            }
            .listStyle(GroupedListStyle())
            .navigationTitle("Accent Colours")
            .navigationBarTitleDisplayMode(.large)
        }
    }
}

struct AccentColorItem: View {
    let name: String
    let color: Color
    
    var body: some View {
        HStack {
            Circle()
                .fill(color)
                .frame(width: 16, height: 16)
            Text(name)
            Spacer()
            if color == .blue {
                Image(systemName: "checkmark")
                    .foregroundColor(.blue)
            }
        }
        .contentShape(Rectangle())
    }
}

struct AccentColourView_Previews: PreviewProvider {
    static var previews: some View {
        let accentColorSelection = AccentColorSelection(selectedColor: .red)
        return AccentColourView(accentColorOptions: [("Red", Color.red),
                                                     ("Blue", Color.blue),
                                                     ("Yellow", Color.yellow),
                                                     ("Orange", Color.orange),
                                                     ("Purple", Color.purple),
                                                     ("Green", Color.green)])
            .environmentObject(accentColorSelection)
    }
}

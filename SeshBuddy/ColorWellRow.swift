//
//  ColorWellRow.swift
//  SeshBuddy
//
//  Created by Jake Gibbons on 26/06/2023.
//

import SwiftUI

struct ColorWellRow: View {
    @State private var accentColor = Color.accentColor
    
    var body: some View {
        VStack(alignment: .leading) {
            ColorPicker("Select A Color", selection: $accentColor)
                .padding(.vertical, 8)
                .accentColor(accentColor)
        }
  }
}

struct ColorWellRow_Previews: PreviewProvider {
    static var previews: some View {
        ColorWellRow()
    }
}

import SwiftUI
import Combine
import AuthenticationServices

class AccentColorDelegateClass: ObservableObject, AccentColorDelegate {
    @Published var selectedAccentColor: Color = .blue
    
    func didSelectAccentColor(_ color: Color) {
        selectedAccentColor = color
    }
}

struct WelcomeView: View {
    @Environment(\.colorScheme) var colorScheme
    @State private var animationAmount: CGFloat = 1
    @State private var infoSheet = false
    @State private var navigateToSettings = false
    
    // Create an instance of the delegate class
    let accentColorDelegate = AccentColorDelegateClass()
    
    var body: some View {
        NavigationStack {
            VStack {
                Spacer()
                Image("AppLogo")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .padding(.horizontal, 50)
                    .padding(.bottom, 20)
                Text("Placeholder tagline")
                    .font(.subheadline)
                Spacer()
                
                
                HStack{
                    Spacer()
                    NavigationLink(destination: LoginView()) {
                        Text("Sign In")
                            .font(.title3)
                            .frame(maxWidth: .infinity)
                            .padding(.vertical, 7)
                    }
                    .buttonBorderShape(.capsule)
                    .buttonStyle(.bordered)
                    .padding(.vertical)
                    .padding(.leading)
                    
                    Spacer()
                    
                    NavigationLink(destination: SignupView()) {
                        Text("Sign Up")
                            .font(.title3)
                            .frame(maxWidth: .infinity)
                            .padding(.vertical, 7)
                    }
                    .buttonBorderShape(.capsule)
                    .buttonStyle(.borderedProminent)
                    .padding(.vertical)
                    .padding(.trailing)
                    
                    Spacer()
                }
                
                SignInWithAppleButton(.continue, onRequest: { request in
                    request.requestedScopes = [.fullName, .email]
                }, onCompletion: { result in
                    switch result {
                    case .success(_):
                        print("Authorization Successful")
                    case .failure(let error):
                        print("Authorization Failure: " + error.localizedDescription)
                    }
                })
                .signInWithAppleButtonStyle(colorScheme == .dark ? .white : .black)
                .frame(height: 50)
                .frame(minWidth: 140, minHeight: 44)
                .cornerRadius(.greatestFiniteMagnitude)
                .padding(.horizontal, 15)
                
                Spacer()
                
                HStack {
                    Text("Built with")
                    ZStack {
                        Image(systemName: "heart")
                            .foregroundColor(.red)
                            .scaleEffect(animationAmount)
                            .animation(
                                Animation.spring(response: 0.2, dampingFraction: 0.3, blendDuration: 0.8)
                                    .delay(0.02)
                                    .repeatForever(autoreverses: true),
                                value: animationAmount
                            )
                            .onAppear {
                                animationAmount = 1.2
                            }
                    }
                    Text("by Jake")
                }
                
                Spacer()
                
            }
            .navigationBarItems(trailing:
                Button(action: {
                    navigateToSettings = true
                }) {
                    Image(systemName: "gearshape")
                }
            )
            .navigationBarItems(trailing:
                Button(action: {
                    infoSheet = true
                }) {
                    Image(systemName: "info.circle")
                }
            )
            .sheet(isPresented: $infoSheet) {
                InfoSheetView(termsAccepted: .constant(false))
            }
            .background(
                NavigationLink(destination: SettingsView(), isActive: $navigateToSettings) {
                    EmptyView()
                }
                .hidden()
            )
        }
        .accentColor(accentColorDelegate.selectedAccentColor)
        .environmentObject(accentColorDelegate) // Set the delegate instance
    }
}


struct WelcomeView_Previews: PreviewProvider {
    static var previews: some View {
        WelcomeView()
    }
}

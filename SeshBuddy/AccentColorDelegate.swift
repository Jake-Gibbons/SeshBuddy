//
//  AccentColorDelegate.swift
//  SeshBuddy
//
//  Created by Jake Gibbons on 28/06/2023.
//

import SwiftUI

protocol AccentColorDelegate: AnyObject {
    func didSelectAccentColor(_ color: Color)
}
